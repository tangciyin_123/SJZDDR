package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.CodeSpecific;
import com.ruoyi.system.service.ICodeSpecificService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 标准字段管理Controller
 * 
 * @author ruoyi
 * @date 2020-02-15
 */
@Controller
@RequestMapping("/system/specific")
public class CodeSpecificController extends BaseController
{
    private String prefix = "system/specific";

    @Autowired
    private ICodeSpecificService codeSpecificService;

    @RequiresPermissions("system:specific:view")
    @GetMapping()
    public String specific()
    {
        return prefix + "/specific";
    }

    /**
     * 查询标准字段管理列表
     */
    @RequiresPermissions("system:specific:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CodeSpecific codeSpecific)
    {
        startPage();
        List<CodeSpecific> list = codeSpecificService.selectCodeSpecificList(codeSpecific);
        return getDataTable(list);
    }

    /**
     * 导出标准字段管理列表
     */
    @RequiresPermissions("system:specific:export")
    @Log(title = "标准字段管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CodeSpecific codeSpecific)
    {
        List<CodeSpecific> list = codeSpecificService.selectCodeSpecificList(codeSpecific);
        ExcelUtil<CodeSpecific> util = new ExcelUtil<CodeSpecific>(CodeSpecific.class);
        return util.exportExcel(list, "specific");
    }

    /**
     * 新增标准字段管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存标准字段管理
     */
    @RequiresPermissions("system:specific:add")
    @Log(title = "标准字段管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CodeSpecific codeSpecific)
    {
        return toAjax(codeSpecificService.insertCodeSpecific(codeSpecific));
    }

    /**
     * 修改标准字段管理
     */
    @GetMapping("/edit/{code}")
    public String edit(@PathVariable("code") String code, ModelMap mmap)
    {
        CodeSpecific codeSpecific = codeSpecificService.selectCodeSpecificById(code);
        mmap.put("codeSpecific", codeSpecific);
        return prefix + "/edit";
    }

    /**
     * 修改保存标准字段管理
     */
    @RequiresPermissions("system:specific:edit")
    @Log(title = "标准字段管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CodeSpecific codeSpecific)
    {
        return toAjax(codeSpecificService.updateCodeSpecific(codeSpecific));
    }

    /**
     * 删除标准字段管理
     */
    @RequiresPermissions("system:specific:remove")
    @Log(title = "标准字段管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(codeSpecificService.deleteCodeSpecificByIds(ids));
    }
}
