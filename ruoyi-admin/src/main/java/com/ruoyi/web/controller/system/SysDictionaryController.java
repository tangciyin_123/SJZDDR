package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysDictionary;
import com.ruoyi.system.service.ISysDictionaryService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 标准字段Controller
 * 
 * @author ruoyi
 * @date 2020-01-30
 */
@Controller
@RequestMapping("/system/dictionary")
public class SysDictionaryController extends BaseController
{
    private String prefix = "system/dictionary";
    @Autowired
    private ISysDictionaryService sysDictionaryService;

    @RequiresPermissions("system:dictionary:view")
    @GetMapping()
    public String dictionary()
    {
        return prefix + "/dictionary";
    }

    /**
     * 查询标准字段列表
     */
    @RequiresPermissions("system:dictionary:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysDictionary sysDictionary)
    {
        startPage();
        List<SysDictionary> list = sysDictionaryService.selectSysDictionaryList(sysDictionary);
        return getDataTable(list);
    }

    /**
     * 导出标准字段列表
     */
    @RequiresPermissions("system:dictionary:export")
    @Log(title = "标准字段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysDictionary sysDictionary)
    {
        List<SysDictionary> list = sysDictionaryService.selectSysDictionaryList(sysDictionary);
        ExcelUtil<SysDictionary> util = new ExcelUtil<SysDictionary>(SysDictionary.class);
        return util.exportExcel(list, "dictionary");
    }

    /**
     * 新增标准字段
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存标准字段
     */
    @RequiresPermissions("system:dictionary:add")
    @Log(title = "标准字段", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysDictionary sysDictionary)
    {
        return toAjax(sysDictionaryService.insertSysDictionary(sysDictionary));
    }

    /**
     * 修改标准字段
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysDictionary sysDictionary = sysDictionaryService.selectSysDictionaryById(id);
        mmap.put("sysDictionary", sysDictionary);
        return prefix + "/edit";
    }

    /**
     * 修改保存标准字段
     */
    @RequiresPermissions("system:dictionary:edit")
    @Log(title = "标准字段", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysDictionary sysDictionary)
    {
        return toAjax(sysDictionaryService.updateSysDictionary(sysDictionary));
    }

    /**
     * 删除标准字段
     */
    @RequiresPermissions("system:dictionary:remove")
    @Log(title = "标准字段", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysDictionaryService.deleteSysDictionaryByIds(ids));
    }
}
