package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysDictionary;
import java.util.List;

/**
 * 数据字典Mapper接口
 * 
 * @author ruoyi
 * @date 2020-01-30
 */
public interface SysDictionaryMapper 
{
    /**
     * 查询数据字典
     * 
     * @param id 数据字典ID
     * @return 数据字典
     */
    public SysDictionary selectSysDictionaryById(Long id);

    /**
     * 查询数据字典列表
     * 
     * @param sysDictionary 数据字典
     * @return 数据字典集合
     */
    public List<SysDictionary> selectSysDictionaryList(SysDictionary sysDictionary);

    /**
     * 新增数据字典
     * 
     * @param sysDictionary 数据字典
     * @return 结果
     */
    public int insertSysDictionary(SysDictionary sysDictionary);

    /**
     * 修改数据字典
     * 
     * @param sysDictionary 数据字典
     * @return 结果
     */
    public int updateSysDictionary(SysDictionary sysDictionary);

    /**
     * 删除数据字典
     * 
     * @param id 数据字典ID
     * @return 结果
     */
    public int deleteSysDictionaryById(Long id);

    /**
     * 批量删除数据字典
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysDictionaryByIds(String[] ids);
}
