package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据字典对象 sys_dictionary
 * 
 * @author ruoyi
 * @date 2020-01-30
 */
public class SysDictionary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 标签 */
    @Excel(name = "标签")
    private String islabel;

    /** 中文描述 */
    @Excel(name = "中文描述")
    private String isname;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setIslabel(String islabel) 
    {
        this.islabel = islabel;
    }

    public String getIslabel() 
    {
        return islabel;
    }
    public void setIsname(String isname) 
    {
        this.isname = isname;
    }

    public String getIsname() 
    {
        return isname;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("islabel", getIslabel())
            .append("isname", getIsname())
            .toString();
    }
}
