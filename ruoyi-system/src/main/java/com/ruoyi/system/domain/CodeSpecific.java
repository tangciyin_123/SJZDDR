package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 标准字段管理对象 code_specific
 * 
 * @author ruoyi
 * @date 2020-02-15
 */
public class CodeSpecific extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标签 */
    @Excel(name = "标签")
    private String code;

    /** 中文描述 */
    @Excel(name = "中文描述")
    private String name;

    /** $column.columnComment */
    @Excel(name = "中文描述")
    private String spell;

    /** 中文首字母大写缩写 */
    @Excel(name = "中文首字母大写缩写")
    private String mapping;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String cxfw;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String bdfw;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String wjfw;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String bkfw;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String xxgz;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String lbdm;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private String lbmc;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private Long lev;

    /** $column.columnComment */
    @Excel(name = "中文首字母大写缩写")
    private Long child;

    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setSpell(String spell) 
    {
        this.spell = spell;
    }

    public String getSpell() 
    {
        return spell;
    }
    public void setMapping(String mapping) 
    {
        this.mapping = mapping;
    }

    public String getMapping() 
    {
        return mapping;
    }
    public void setCxfw(String cxfw) 
    {
        this.cxfw = cxfw;
    }

    public String getCxfw() 
    {
        return cxfw;
    }
    public void setBdfw(String bdfw) 
    {
        this.bdfw = bdfw;
    }

    public String getBdfw() 
    {
        return bdfw;
    }
    public void setWjfw(String wjfw) 
    {
        this.wjfw = wjfw;
    }

    public String getWjfw() 
    {
        return wjfw;
    }
    public void setBkfw(String bkfw) 
    {
        this.bkfw = bkfw;
    }

    public String getBkfw() 
    {
        return bkfw;
    }
    public void setXxgz(String xxgz) 
    {
        this.xxgz = xxgz;
    }

    public String getXxgz() 
    {
        return xxgz;
    }
    public void setLbdm(String lbdm) 
    {
        this.lbdm = lbdm;
    }

    public String getLbdm() 
    {
        return lbdm;
    }
    public void setLbmc(String lbmc) 
    {
        this.lbmc = lbmc;
    }

    public String getLbmc() 
    {
        return lbmc;
    }
    public void setLev(Long lev) 
    {
        this.lev = lev;
    }

    public Long getLev() 
    {
        return lev;
    }
    public void setChild(Long child) 
    {
        this.child = child;
    }

    public Long getChild() 
    {
        return child;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("code", getCode())
            .append("name", getName())
            .append("spell", getSpell())
            .append("mapping", getMapping())
            .append("cxfw", getCxfw())
            .append("bdfw", getBdfw())
            .append("wjfw", getWjfw())
            .append("bkfw", getBkfw())
            .append("xxgz", getXxgz())
            .append("lbdm", getLbdm())
            .append("lbmc", getLbmc())
            .append("lev", getLev())
            .append("child", getChild())
            .toString();
    }
}
