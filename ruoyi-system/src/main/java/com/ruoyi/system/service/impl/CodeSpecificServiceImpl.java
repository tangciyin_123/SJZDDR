package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CodeSpecificMapper;
import com.ruoyi.system.domain.CodeSpecific;
import com.ruoyi.system.service.ICodeSpecificService;
import com.ruoyi.common.core.text.Convert;

/**
 * 标准字段管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-02-15
 */
@Service
public class CodeSpecificServiceImpl implements ICodeSpecificService 
{
    @Autowired
    private CodeSpecificMapper codeSpecificMapper;

    /**
     * 查询标准字段管理
     * 
     * @param code 标准字段管理ID
     * @return 标准字段管理
     */
    @Override
    public CodeSpecific selectCodeSpecificById(String code)
    {
        return codeSpecificMapper.selectCodeSpecificById(code);
    }

    /**
     * 查询标准字段管理列表
     * 
     * @param codeSpecific 标准字段管理
     * @return 标准字段管理
     */
    @Override
    public List<CodeSpecific> selectCodeSpecificList(CodeSpecific codeSpecific)
    {
        return codeSpecificMapper.selectCodeSpecificList(codeSpecific);
    }

    /**
     * 新增标准字段管理
     * 
     * @param codeSpecific 标准字段管理
     * @return 结果
     */
    @Override
    public int insertCodeSpecific(CodeSpecific codeSpecific)
    {
        return codeSpecificMapper.insertCodeSpecific(codeSpecific);
    }

    /**
     * 修改标准字段管理
     * 
     * @param codeSpecific 标准字段管理
     * @return 结果
     */
    @Override
    public int updateCodeSpecific(CodeSpecific codeSpecific)
    {
        return codeSpecificMapper.updateCodeSpecific(codeSpecific);
    }

    /**
     * 删除标准字段管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCodeSpecificByIds(String ids)
    {
        return codeSpecificMapper.deleteCodeSpecificByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除标准字段管理信息
     * 
     * @param code 标准字段管理ID
     * @return 结果
     */
    @Override
    public int deleteCodeSpecificById(String code)
    {
        return codeSpecificMapper.deleteCodeSpecificById(code);
    }
}
