package com.ruoyi.system.service.impl;

import com.csvreader.CsvReader;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;

//读取文件 -》判断文件类型-》读取内容

public class JudgementFile {

    //读取文件名
 //   @RequestMapping("/file")
    public String  getFileName(String fileName){
        return fileName;
    }

    //判断文件类型
    public String getTypeFile(){
        String name=getFileName("fileName");
        if(name!=null){


        }

        return "";
    }
    //读取文件内容  ---三种类型的调用方法 Excel  csv  json
//csv调用的方法
    public static Object[][] readCSV(JudgementFile csv) {
        //try{业务代码}catch(Exception e){如果做业务的过程中出了错，的异常处理逻辑}
        try {
            String csvFilePath=csv.getTypeFile();
            //容器：对象少的时候，直接把对象列出来；当对象很多的时候，要用一个容器装起来打包
            ArrayList<String[]> csvFileList = new ArrayList<String[]>();
            // 这个不用背，只要看得懂会用就行。创建CSV读对象 例如:CsvReader(文件路径，分隔符，编码格式);

            CsvReader reader = new CsvReader(csvFilePath, ',', Charset.forName("GBK"));
            // 跳过表头 如果需要表头的话，这句可以忽略
            // reader.readHeaders();
            // 逐行读入除表头的数据
            //boolean变量：真假true或者false
            int count=0;
            while (reader.readRecord()) {
                count++;
                System.out.println(reader.getRawRecord());
                //将一行的字符串按照“，”逗号分成多列，存放到String[]数组中
                //再将这个string[]放到list容器中存起来
                if(count<=50){
                    csvFileList.add(reader.getValues());
                }else {
                    return null;
                }
            }
            //数据取完了，关闭文件
            reader.close();
          // 遍历读取的CSV文件
            //for是一个整数次的循环，三个参数：最小值，最大值，增量，取个变量名存放每次循环的序列值

            Object[][] result=new Object[csvFileList.size()][csvFileList.get(0).length];
            for (int row = 0; row <csvFileList.size(); row++) {
                result[row]=csvFileList.get(row);
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //调用读取excel方法


}
