package com.ruoyi.system.service;

import com.ruoyi.system.domain.CodeSpecific;
import java.util.List;

/**
 * 标准字段管理Service接口
 * 
 * @author ruoyi
 * @date 2020-02-15
 */
public interface ICodeSpecificService 
{
    /**
     * 查询标准字段管理
     * 
     * @param code 标准字段管理ID
     * @return 标准字段管理
     */
    public CodeSpecific selectCodeSpecificById(String code);

    /**
     * 查询标准字段管理列表
     * 
     * @param codeSpecific 标准字段管理
     * @return 标准字段管理集合
     */
    public List<CodeSpecific> selectCodeSpecificList(CodeSpecific codeSpecific);

    /**
     * 新增标准字段管理
     * 
     * @param codeSpecific 标准字段管理
     * @return 结果
     */
    public int insertCodeSpecific(CodeSpecific codeSpecific);

    /**
     * 修改标准字段管理
     * 
     * @param codeSpecific 标准字段管理
     * @return 结果
     */
    public int updateCodeSpecific(CodeSpecific codeSpecific);

    /**
     * 批量删除标准字段管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCodeSpecificByIds(String ids);

    /**
     * 删除标准字段管理信息
     * 
     * @param code 标准字段管理ID
     * @return 结果
     */
    public int deleteCodeSpecificById(String code);
}
