package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysDictionaryMapper;
import com.ruoyi.system.domain.SysDictionary;
import com.ruoyi.system.service.ISysDictionaryService;
import com.ruoyi.common.core.text.Convert;

/**
 * 数据字典Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-01-30
 */
@Service
public class SysDictionaryServiceImpl implements ISysDictionaryService 
{
    @Autowired
    private SysDictionaryMapper sysDictionaryMapper;

    /**
     * 查询数据字典
     * 
     * @param id 数据字典ID
     * @return 数据字典
     */
    @Override
    public SysDictionary selectSysDictionaryById(Long id)
    {
        return sysDictionaryMapper.selectSysDictionaryById(id);
    }

    /**
     * 查询数据字典列表
     * 
     * @param sysDictionary 数据字典
     * @return 数据字典
     */
    @Override
    public List<SysDictionary> selectSysDictionaryList(SysDictionary sysDictionary)
    {
        return sysDictionaryMapper.selectSysDictionaryList(sysDictionary);
    }

    /**
     * 新增数据字典
     * 
     * @param sysDictionary 数据字典
     * @return 结果
     */
    @Override
    public int insertSysDictionary(SysDictionary sysDictionary)
    {
        return sysDictionaryMapper.insertSysDictionary(sysDictionary);
    }

    /**
     * 修改数据字典
     * 
     * @param sysDictionary 数据字典
     * @return 结果
     */
    @Override
    public int updateSysDictionary(SysDictionary sysDictionary)
    {
        return sysDictionaryMapper.updateSysDictionary(sysDictionary);
    }

    /**
     * 删除数据字典对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysDictionaryByIds(String ids)
    {
        return sysDictionaryMapper.deleteSysDictionaryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除数据字典信息
     * 
     * @param id 数据字典ID
     * @return 结果
     */
    @Override
    public int deleteSysDictionaryById(Long id)
    {
        return sysDictionaryMapper.deleteSysDictionaryById(id);
    }
}
