﻿
DROP TABLE IF EXISTS `CODE_SPECIFIC`;
CREATE TABLE `CODE_SPECIFIC` (
  `code` varchar(20) CHARACTER SET utf8 NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `spell` varchar(200) DEFAULT NULL,
  `mapping` varchar(20) DEFAULT NULL,
  `cxfw` varchar(1) DEFAULT NULL,
  `bdfw` varchar(1) DEFAULT NULL,
  `wjfw` varchar(1) DEFAULT NULL,
  `bkfw` varchar(1) DEFAULT NULL,
  `xxgz` varchar(1) DEFAULT NULL,
  `lbdm` varchar(2) DEFAULT NULL,
  `lbmc` varchar(100) DEFAULT NULL,
  `lev` decimal(1,0) DEFAULT '1',
  `child` decimal(1,0) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxddmc', '出现地点名称', 'CXDDMC', 'CXDDMC', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ajbh', '案件编号', null, 'AJBH', '1', '0', '0', '0', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ajlb', '案件类别', null, 'AJLB', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cbajbh', '串并案件编号', null, 'CBAJBH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cph', '机动车车牌号', null, 'CPH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-csrq', '出生日期', null, 'CSRQ', '1', '1', '1', '1', '1', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxcsmc', '出现场所名称', null, 'CXCSMC', '1', '1', '1', '1', '1', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxddqh', '出现地点区划', null, 'CXDDQH', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxjssj', '出现结束时间', null, 'CXJSSJ', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxkssj', '出现开始时间', null, 'CXKSSJ', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ddzxccjh', '电动自行车车架号', null, 'DDZXCCJH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ddzxccph', '电动自行车车牌号', null, 'DDZXCCPH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ddzxcdjh', '电动自行车电机号', null, 'DDZXCDJH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ddzxcgyh', '电动自行车钢印号', null, 'DDZXCGYH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-dhhm', '电话号码', null, 'DHHM', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-dizhi', '人员地址', null, 'RYDZ', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-dnabh', 'DNA编号', null, 'DNABH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-dwbh', '单位编号', null, 'DWBH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-email', 'EMAIL', null, 'EMAIL', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-gjbh', '轨迹编号', null, 'GJBH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-hjdxzqh', '户籍地行政区划', null, 'HJDQH', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-iccid', '集成电路卡识别码', null, 'ICCID', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-imei', '手机串号', null, 'IMEI', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-imsi', 'SIM卡号', null, 'IMSI', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-jdccjh', '机动车车架号', null, 'CJH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-jdcvin', '机动车发动机号', null, 'VIN', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-labs', '类案标识', null, 'LABS', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxcsbh', '出现场所编号', null, 'CXCSBH', '1', '1', '1', '1', '1', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-cxcsnjtwzhm', '出现场所内具体位置号码', null, 'CXCSNJTWZHM', '1', '1', '1', '1', '1', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-mmdxzqh', '目的地行政区划', null, 'MDDXZQH', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-mz', '民族', null, 'MZ', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-pc-mac', 'MAC地址', null, 'MAC', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-qh', '枪号', null, 'QH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-qq', 'QQ号码', null, 'QQHM', '1', '0', '0', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-qtbh', '群体编号', null, 'QTBH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-rwbh', '任务编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-rybh', '人员编号', null, 'RYBH', '1', '0', '0', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-sfzh', '身份证号', null, 'SFZH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-shengao', '身高', null, null, '0', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-sjlb', '事件类别', null, null, '0', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-szrybh', '失踪人员编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-szzwbh', '十指指纹编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-thbh', '团伙编号', null, 'THBH', '1', '0', '0', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-tzc', '特征词', null, null, '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wdbh', '文档编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wffzrybh', '违法犯罪人员编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wpbh', '物品编号', null, 'WPBH', '1', '0', '0', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wplb', '物品类别', null, null, '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wpxlh', '物品序列号', null, 'WPXLH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wzbh', '物证编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-wzmstbh', '未知名尸体编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-xb', '性别', null, 'XB', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-xckybh', '现场勘验编号', null, null, '1', '0', '0', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-xm', '姓名', null, 'XM', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-xsbh', '线索编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-yhzh', '银行帐号', null, 'YHKH', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-zjhm', '证件号码', null, 'ZJHM', '1', '1', '1', '1', '1', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ztc', '主题词', null, null, '0', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-ztrybh', '在逃人员编号', null, null, '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-zzjgdm', '组织机构代码', null, 'ZZJGDM', '1', '0', '0', '0', '0', '02', '条件标识', 1, 0);

insert into CODE_SPECIFIC (CODE, NAME, SPELL, MAPPING, CXFW, BDFW, WJFW, BKFW, XXGZ, LBDM, LBMC, LEV, CHILD)
values ('is-hh', '户号', 'HH', 'HH', '1', '0', '1', '0', '0', '01', '实体标识', 1, 0);

